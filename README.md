![azureorange404](https://gitlab.com/azureorange404/azureorange404/-/raw/main/assets/img/header.png)

# AzureOrange404

- 👋 Hi, I’m @AzureOrange404, but just call me Azure. 
- 👀 I’m interested in GNU/Linux, self-hosting, and decentralization 
- 📫 You may reach me via my email address: azureorange404@protonmail.com or by submitting an issue to this repository.

## Repositories

[dotfiles](https://gitlab.com/azureorange404/dotfiles)
my configuration files.

[dmenu-scripts](https://gitlab.com/azureorange404/dmenu-scripts) 
a collection of bash scripts to use with dmenu.

[shell-scripts](https://gitlab.com/azureorange404/shell-scripts) 
a collection of various shell scripts.

[wallpapers](https://gitlab.com/azureorange404/wallpapers)
the collection of wallpapers I use.

[latex](https://gitlab.com/azureorange404/latex)
latex templates of mine.

[website](https://gitlab.com/azureorange404/azureorange.xyz)
my personal website --> [azureorange.xyz](https://azureorange.graspingspriggan.ch/).

## Forked Projects

[dmenu](https://gitlab.com/azureorange404/dmenu)
my fork of dmenu (applied some patches)

[st](https://gitlab.com/azureorange404/st)
my fork of the suckless terminal (again some patches applied)

[passmenu-otp](https://gitlab.com/azureorange404/passmenu-otp)
dmenu integration of pass and pass-otp.


# RickDangerous

The RickDangerous Project is simply put a collection of games and themes for the RetroPi.

## Repositories

[wiki website](https://gitlab.com/rickdangerous/DangerousWikiWebsite)

[wiki gemini capsule](https://gitlab.com/rickdangerous/DangerousWikiCapsule)

# SchoolStuff

SchoolStuff will be a place where you will find various kinds of materials to use if you are a teacher.
All contributions are welcome.

## Repositories

[website](https://gitlab.com/schoolstuff404/schoolstuff)
<!---
```markdown
Syntax highlighted code block

# Header 1
## Header 2
### Header 3

- Bulleted
- List

1. Numbered
2. List

**Bold** and _Italic_ and `Code` text

[Link](url) and ![Image](src)
```
--->
